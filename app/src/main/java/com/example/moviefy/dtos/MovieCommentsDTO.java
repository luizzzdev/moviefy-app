package com.example.moviefy.dtos;

import com.example.moviefy.entities.Category;
import com.example.moviefy.entities.Comment;

import java.util.ArrayList;
import java.util.List;

public class MovieCommentsDTO {
    private Long id;
    private String name;
    private String releaseDate;
    private String photoUrl;
    private String description;
    private Double rates;

    private List<Category> categories = new ArrayList<>();
    private List<Comment> comments = new ArrayList<>();

    public MovieCommentsDTO() {
    }

    public MovieCommentsDTO(Long id, String name, String releaseDate, String photoUrl, String description, List<Comment> comments, Double rates) {
        this.id = id;
        this.name = name;
        this.releaseDate = releaseDate;
        this.photoUrl = photoUrl;
        this.description = description;
        this.comments = comments;
        this.rates = rates;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRates(Double rates) { this.rates = rates; }

    public Double getRates() { return rates; }

    public List<Category> getCategories() {
        return categories;
    }

    public List<Comment> getComments() {
        return comments;
    }

}
