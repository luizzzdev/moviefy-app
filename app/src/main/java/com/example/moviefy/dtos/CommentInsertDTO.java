package com.example.moviefy.dtos;

public class CommentInsertDTO {
    private String text;

    public CommentInsertDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
