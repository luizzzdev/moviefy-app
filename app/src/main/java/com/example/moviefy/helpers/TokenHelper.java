package com.example.moviefy.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class TokenHelper {
    private Context context;

    public TokenHelper(Context context) {
        this.context = context;
    }

    public String getToken() {
        SharedPreferences sp = context.getSharedPreferences("dados", 0);
        String token = sp.getString("token", null);
        if (token == null) return null;
        return "Bearer " + token;
    }

    public void cleanToken() {
        SharedPreferences sp = context.getSharedPreferences("dados", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("token", null);
        editor.apply();
    }
}
