package com.example.moviefy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.moviefy.R;
import com.example.moviefy.adapters.CommentAdapter;
import com.example.moviefy.dtos.CommentInsertDTO;
import com.example.moviefy.dtos.MovieCommentsDTO;
import com.example.moviefy.entities.Comment;
import com.example.moviefy.entities.Movie;
import com.example.moviefy.entities.Rate;
import com.example.moviefy.helpers.TokenHelper;
import com.example.moviefy.services.RetrofitService;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieActivity extends AppCompatActivity {
    private static final String TAG = "MovieActivity";
    private TextView movieName;
    private MovieCommentsDTO movie;
    private ImageButton sendButton;
    TextWatcher commentWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.toString().trim().length() == 0) {
                sendButton.setEnabled(false);
                return;
            }
            sendButton.setEnabled(true);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };
    private EditText movieComment;
    private TokenHelper tokenHelper;
    private ImageView movieImage;
    private TextView movieDescription;
    private TextView movieDate;
    private RatingBar ratingBar;
    private TextView ratingValue;
    private RatingBar myRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        tokenHelper = new TokenHelper(this);

        movieName = findViewById(R.id.movie_name);
        sendButton = findViewById(R.id.send_comment_button);
        sendButton.setEnabled(false);
        movieComment = findViewById(R.id.movie_comment);
        movieImage = findViewById(R.id.movie_image);
        movieDate = findViewById(R.id.movie_date);
        movieDescription = findViewById(R.id.movie_desc);
        ratingValue = findViewById(R.id.ratingValue);
        myRate = findViewById(R.id.rating);
        movieComment.addTextChangedListener(commentWatcher);
        getMovie();
        addListenerOnRatingBar();

    }

    public void addListenerOnRatingBar() {
        ratingBar = findViewById(R.id.rating);
        ratingValue = findViewById(R.id.ratingValue);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                rateMovie(rating);
            }
        });
    }

    private void getMovie() {
        Long id = getIntent().getLongExtra("movieId", -1);

        if (id == -1) return;

        RetrofitService.getService(MovieActivity.this).getMovie(id).enqueue(new Callback<MovieCommentsDTO>() {
            @Override
            public void onResponse(Call<MovieCommentsDTO> call, Response<MovieCommentsDTO> response) {
                movie = response.body();
                movieName.setText(movie.getName());
                movieDescription.setText(movie.getDescription());
                movieDate.setText(movie.getReleaseDate());
                ratingValue.setText(String.valueOf(movie.getRates()));

                Picasso.get().load(movie.getPhotoUrl()).into(movieImage);
                initCommentsRecyclerView();
            }

            @Override
            public void onFailure(Call<MovieCommentsDTO> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });

        RetrofitService.getService(MovieActivity.this).getMyRate(id, tokenHelper.getToken()).enqueue(new Callback<Rate>() {
            @Override
            public void onResponse(Call<Rate> call, Response<Rate> response) {

                if (response.code() == 403) {
                    tokenHelper.cleanToken();
                    startActivity(new Intent(MovieActivity.this, LoginActivity.class));
                    return;
                }

                if (response.code() == 200) {
                    myRate.setRating(Float.parseFloat(response.body().getRate().toString()));
                }
            }

            @Override
            public void onFailure(Call<Rate> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });

    }

    private void initCommentsRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.rv_movie_comments);
        CommentAdapter commentAdapter = new CommentAdapter(this, movie.getComments());
        recyclerView.setAdapter(commentAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void sendComment(View view) {
        String token = tokenHelper.getToken();

        if (token == null) startActivity(new Intent(MovieActivity.this, LoginActivity.class));

        this.commentMovie();
    }

    private void commentMovie() {
        Long movieId = this.movie.getId();
        CommentInsertDTO comment = new CommentInsertDTO(movieComment.getText().toString());

        RetrofitService.getService(MovieActivity.this).commentMovie(movieId, comment, tokenHelper.getToken()).enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                if (response.code() == 201) {
                    movieComment.setText("");
                    getMovie();
                    return;
                }

                if (response.code() == 403) {
                    tokenHelper.cleanToken();
                    startActivity(new Intent(MovieActivity.this, LoginActivity.class));
                    return;
                }
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {

            }
        });

    }

    private void rateMovie(Float rateValue) {
        if (this.movie == null) return;

        Long movieId = this.movie.getId();
        int rate = Math.round(rateValue);

        RetrofitService.getService(MovieActivity.this).rateMovie(movieId, rate, tokenHelper.getToken()).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.code() == 403) {
                    tokenHelper.cleanToken();
                    startActivity(new Intent(MovieActivity.this, LoginActivity.class));
                    return;
                }
                getMovie();
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
            }
        });
    }

}
