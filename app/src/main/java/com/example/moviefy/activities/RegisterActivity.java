package com.example.moviefy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moviefy.R;
import com.example.moviefy.dtos.DtoUser;
import com.example.moviefy.services.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
    }

    public void register(View view) {
        String name = ((EditText) findViewById(R.id.register_name)).getText().toString();
        String email = ((EditText) findViewById(R.id.register_email)).getText().toString();
        String password = ((EditText) findViewById(R.id.register_pass)).getText().toString();
        String confPassword = ((EditText) findViewById(R.id.register_conf_pass)).getText().toString();

        DtoUser dtoUser = new DtoUser();
        dtoUser.setEmail(email);
        dtoUser.setName(name);
        dtoUser.setPassword(password);
        dtoUser.setPhone("99999999999");

        if (!confPassword.equals(password)) {
            Toast.makeText(RegisterActivity.this, "As senhas não conferem. Tente novamente.", Toast.LENGTH_SHORT).show();
            return;
        }


        RetrofitService.getService(this).register(dtoUser).enqueue(new Callback<DtoUser>() {
            @Override
            public void onResponse(Call<DtoUser> call, Response<DtoUser> response) {
                if (response.code() == 201) {
                    Toast.makeText(RegisterActivity.this, "Usuário cadastrado", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                } else {
                    Toast.makeText(RegisterActivity.this, "Ocorreu um erro ao cadastrar este usuário.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DtoUser> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public void openLoginPage(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }
}
