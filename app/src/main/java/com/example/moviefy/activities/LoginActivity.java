package com.example.moviefy.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moviefy.R;
import com.example.moviefy.dtos.DtoLogin;
import com.example.moviefy.services.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    }

    public void login(View view) {
        String email = ((EditText) findViewById(R.id.email_login)).getText().toString();
        String password = ((EditText) findViewById(R.id.password_login)).getText().toString();

        DtoLogin dtoLogin = new DtoLogin();
        dtoLogin.setEmail(email);
        dtoLogin.setPassword(password);

        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Informe um usuário válido.", Toast.LENGTH_SHORT).show();
            return;
        } else {
            RetrofitService.getService(this).login(dtoLogin).enqueue(new Callback<DtoLogin>() {
                @Override
                public void onResponse(Call<DtoLogin> call, Response<DtoLogin> response) {
                    if (response.code() == 200) {
                        String token = response.body().getToken();
                        Toast.makeText(LoginActivity.this, "Usuário logado", Toast.LENGTH_SHORT).show();
                        SharedPreferences sp = getSharedPreferences("dados", 0);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("token", token);
                        editor.apply();
                    } else {
                        Toast.makeText(LoginActivity.this, "Usuário ou senha incorretos. Tente novamente.", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<DtoLogin> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
    }

    public void openRegisterPage(View view) {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }
}
