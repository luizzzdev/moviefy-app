package com.example.moviefy.activities.ui.movie;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.moviefy.R;
import com.example.moviefy.adapters.MovieAdapter;
import com.example.moviefy.entities.Movie;
import com.example.moviefy.entities.ResponseEntitity;
import com.example.moviefy.services.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieFragment extends Fragment {
    private static final String TAG = "MovieFragment";
    RecyclerView recyclerView;
    private List<Movie> movies = new ArrayList<>();
    private int page = 0;
    private boolean loading = false;
    MovieAdapter movieAdapter;

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == movies.size() - 1) {
                if (loading) return;
                page += 1;
                getMovies();
            }

        }
    };

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_movies, container, false);
        recyclerView = root.getRootView().findViewById(R.id.rv_movies_list);
        recyclerView.addOnScrollListener(onScrollListener);
        getMovies();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (page == 0) getMovies();
    }

    private void getMovies() {
        loading = true;

        RetrofitService.getService(getContext()).getMovies(page).enqueue(new Callback<ResponseEntitity<Movie>>() {
            @Override
            public void onResponse(Call<ResponseEntitity<Movie>> call, Response<ResponseEntitity<Movie>> response) {
                Log.d(TAG, "GET [/movies]");
                List<Movie> payload = response.body() != null ? response.body().getContent() : null;

                if (payload == null) return;

                if (page == 0) {
                    movies = payload;
                    initRecyclerView();
                } else {
                    movies.addAll(payload);
                    movieAdapter.notifyDataSetChanged();
                }
                loading = false;
            }

            @Override
            public void onFailure(Call<ResponseEntitity<Movie>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
                loading = false;
            }
        });
    }

    private void initRecyclerView() {
        movieAdapter = new MovieAdapter(getContext(), movies);
        recyclerView.setAdapter(movieAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}