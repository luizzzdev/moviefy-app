package com.example.moviefy.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.moviefy.R;
import com.example.moviefy.adapters.MovieAdapter;
import com.example.moviefy.entities.Movie;
import com.example.moviefy.entities.Playlist;
import com.example.moviefy.services.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaylistActivity extends AppCompatActivity {
    private static final String TAG = "PlaylistActivity";
    private List<Movie> movies = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_movies);
        getPlaylist();
    }

    private void getPlaylist() {
        Long id = getIntent().getLongExtra("playlistId", -1);

        if (id == -1) return;

        RetrofitService.getService(PlaylistActivity.this).getPlaylist(id).enqueue(new Callback<Playlist>() {

            @Override
            public void onResponse(Call<Playlist> call, Response<Playlist> response) {
                Log.d(TAG, "GET [/movies]");

                if (response.body() != null) movies.addAll(response.body().getPlaylistMovies());
                initRecyclerView();
            }

            @Override
            public void onFailure(Call<Playlist> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.rv_playlist_movie_list);
        MovieAdapter movieAdapter = new MovieAdapter(this, movies);
        recyclerView.setAdapter(movieAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
