package com.example.moviefy.activities.ui.playlist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.moviefy.R;
import com.example.moviefy.adapters.PlaylistAdapter;
import com.example.moviefy.entities.Playlist;
import com.example.moviefy.entities.ResponseEntitity;
import com.example.moviefy.services.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PlaylistFragment extends Fragment {
    private static final String TAG = "PlaylistFragment";
    RecyclerView recyclerView;
    private List<Playlist> playlists = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_playlists, container, false);
        recyclerView = root.getRootView().findViewById(R.id.rv_playlist);
        getPlaylists();
        return root;
    }

    private void getPlaylists() {
        RetrofitService.getService(getContext()).getPlaylistMovies().enqueue(new Callback<ResponseEntitity<Playlist>>() {
            @Override
            public void onResponse(Call<ResponseEntitity<Playlist>> call, Response<ResponseEntitity<Playlist>> response) {
                Log.d(TAG, "GET [/playlists]");
                List<Playlist> payload = response.body().getContent();

                if (payload != null) playlists.addAll(payload);
                initRecyclerView();
            }

            @Override
            public void onFailure(Call<ResponseEntitity<Playlist>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }

    private void initRecyclerView() {
        PlaylistAdapter playlistAdapter = new PlaylistAdapter(getContext(), playlists);
        recyclerView.setAdapter(playlistAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

}