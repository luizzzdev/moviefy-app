package com.example.moviefy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moviefy.R;
import com.example.moviefy.activities.PlaylistActivity;
import com.example.moviefy.entities.Playlist;

import java.util.ArrayList;
import java.util.List;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlaylistHolder> {
    private Context context;
    private LayoutInflater inflater;
    private List<Playlist> playlists = new ArrayList<>();

    public PlaylistAdapter(Context context, List<Playlist> playlists) {
        this.context = context;
        this.playlists = playlists;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public PlaylistAdapter.PlaylistHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.playlist_item, viewGroup, false);
        return new PlaylistHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistAdapter.PlaylistHolder playlistHolder, int position) {
        final Playlist playlist = playlists.get(position);
        playlistHolder.playlistTitle.setText(playlist.getTitle().toUpperCase());

        playlistHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent playlistIntent = new Intent(context, PlaylistActivity.class);
                playlistIntent.putExtra("playlistId", playlist.getId());
                context.startActivity(playlistIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }

    public class PlaylistHolder extends RecyclerView.ViewHolder {
        public final TextView playlistTitle;

        public PlaylistHolder(@NonNull View itemView) {
            super(itemView);
            playlistTitle = itemView.findViewById(R.id.playlist_title);
        }
    }
}
