package com.example.moviefy.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moviefy.R;
import com.example.moviefy.entities.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentHolder> {
    private Context context;
    private LayoutInflater inflater;
    private List<Comment> comments = new ArrayList<>();

    public CommentAdapter(Context context, List<Comment> comments) {
        this.context = context;
        this.comments = comments;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CommentAdapter.CommentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.comment_item, viewGroup, false);
        return new CommentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.CommentHolder commentHolder, int position) {
        final Comment comment = comments.get(position);
        commentHolder.commentText.setText(comment.getText());
        commentHolder.commentUser.setText(comment.getUserName());
        commentHolder.commentDate.setText(comment.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class CommentHolder extends RecyclerView.ViewHolder {
        public final TextView commentText;
        public final TextView commentUser;
        public final TextView commentDate;

        public CommentHolder(@NonNull View itemView) {
            super(itemView);
            commentText = itemView.findViewById(R.id.comment_text);
            commentUser = itemView.findViewById(R.id.comment_user);
            commentDate = itemView.findViewById(R.id.comment_date);
        }
    }
}
