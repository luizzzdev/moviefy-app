package com.example.moviefy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.moviefy.R;
import com.example.moviefy.activities.MovieActivity;
import com.example.moviefy.entities.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    private Context context;
    private LayoutInflater inflater;
    private List<Movie> movies = new ArrayList<>();

    public MovieAdapter(Context context, List<Movie> movies) {
        this.context = context;
        this.movies = movies;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MovieAdapter.MovieHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.movie_card, viewGroup, false);
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MovieHolder movieHolder, int position) {
        final Movie movie = movies.get(position);
        movieHolder.movieName.setText(movie.getName().toUpperCase());
        movieHolder.movieDate.setText(movie.getReleaseDate());
        movieHolder.movieDescription.setText(movie.getDescription());
        movieHolder.ratingValue.setText(String.valueOf(movie.getRates()));
        Picasso.get().load(movie.getPhotoUrl()).into(movieHolder.movieImage);

        movieHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent movieIntent = new Intent(context, MovieActivity.class);
                movieIntent.putExtra("movieId", movie.getId());
                context.startActivity(movieIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class MovieHolder extends RecyclerView.ViewHolder {
        public final TextView movieName;
        public final ImageView movieImage;
        public final TextView movieDate;
        public final TextView movieDescription;
        public final TextView ratingValue;


        public MovieHolder(@NonNull View itemView) {
            super(itemView);
            movieName = itemView.findViewById(R.id.movie_name);
            movieImage = itemView.findViewById(R.id.movie_image);
            movieDate = itemView.findViewById(R.id.movie_date);
            movieDescription = itemView.findViewById(R.id.movie_desc);
            ratingValue = itemView.findViewById(R.id.cardRatingValue);
        }
    }
}
