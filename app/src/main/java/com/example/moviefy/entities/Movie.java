package com.example.moviefy.entities;

import java.util.HashSet;
import java.util.Set;

public class Movie {
    private Long id;
    private String name;
    private String releaseDate;
    private String photoUrl;
    private String description;
    private Double rates;

    private Set<Category> categories = new HashSet<>();

    public Movie() {
    }

    public Movie(Long id, String name, String releaseDate, String photoUrl, String description, Double rates) {
        this.id = id;
        this.name = name;
        this.releaseDate = releaseDate;
        this.photoUrl = photoUrl;
        this.description = description;
        this.rates = rates;

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setRates(Double rates) { this.rates = rates; }

    public Double getRates() { return rates; }

    public Set<Category> getCategories() {
        return categories;
    }
}
