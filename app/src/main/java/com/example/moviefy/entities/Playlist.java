package com.example.moviefy.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Playlist {
    private Long id;
    private String title;
    private List<Movie> movies = new ArrayList<>();

    public Playlist() {
    }

    public Playlist(Long id, String title, List<Movie> movies) {
        this.id = id;
        this.title = title;
        this.movies = movies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Movie> getPlaylistMovies() {
        return movies;
    }

}
