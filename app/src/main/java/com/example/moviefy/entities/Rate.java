package com.example.moviefy.entities;

public class Rate {

    private Double rate;

    public Rate() {
    }

    public Rate(Double rate) {
        this.rate = rate;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

}
