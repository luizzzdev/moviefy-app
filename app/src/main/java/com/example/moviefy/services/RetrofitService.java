package com.example.moviefy.services;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    private static Context context;
    private String baseUrl = "https://luizzzdev-moviefy.herokuapp.com/";
    private ServiceInterface api;
    private static RetrofitService instancia;

    private RetrofitService() {
        api = criaRetrofit().create(ServiceInterface.class);
    }

    private Retrofit criaRetrofit() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();
    }

    public static ServiceInterface getService(Context context) {
        RetrofitService.context = context;
        if (instancia == null)
            instancia = new RetrofitService();
        return instancia.api;
    }

}