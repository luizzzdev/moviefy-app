package com.example.moviefy.services;

import com.example.moviefy.dtos.CommentInsertDTO;
import com.example.moviefy.dtos.DtoLogin;
import com.example.moviefy.dtos.DtoUser;
import com.example.moviefy.dtos.MovieCommentsDTO;
import com.example.moviefy.entities.Comment;
import com.example.moviefy.entities.Movie;
import com.example.moviefy.entities.Playlist;
import com.example.moviefy.entities.Rate;
import com.example.moviefy.entities.ResponseEntitity;
import com.example.moviefy.helpers.Page;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServiceInterface {
    @POST("/auth/login")
    Call<DtoLogin> login(@Body DtoLogin dtoLogin);

    @POST("/users")
    Call<DtoUser> register(@Body DtoUser dtoUser);

    @GET("/movies")
    Call<ResponseEntitity<Movie>> getMovies(@Query("page") int page);

    @GET("/movies/{id}")
    Call<MovieCommentsDTO> getMovie(@Path("id") Long id);

    @GET("/playlists")
    Call<ResponseEntitity<Playlist>> getPlaylistMovies();

    @GET("/playlists/{id}")
    Call<Playlist> getPlaylist(@Path("id") Long id);

    @POST("/movies/{id}/comment")
    Call<Comment> commentMovie(@Path("id") Long id, @Body CommentInsertDTO commentInsertDTO, @Header("Authorization") String token);

    @POST("/movies/{id}/avaliation")
    Call<Movie> rateMovie(@Path("id") Long id, @Body int rate, @Header("Authorization") String token);

    @GET("/movies/{id}/my-rate")
    Call<Rate> getMyRate(@Path("id") Long id, @Header("Authorization") String token);
}
